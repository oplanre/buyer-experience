---
title: Provide large files to GitLab support
description: How to share large files with the GitLab support team.
support-hero:
  data:
    title: Provide large files to GitLab support
    content: |
      Zendesk has a <a href="https://support.zendesk.com/hc/en-us/articles/4408832757146#topic_lv2_cnx_xdb">maximum attachment size</a> of 50MB per file.
      Zendesk will not allow us to increase this limit any further.
side_menu:
  anchors:
    text: 'On this page'
    data:
      - text: 'Support Uploader'
        href: '#support-uploader'
      - text: "Other methods"
        href: "#other-methods"
        nodes:
          - text: 'Compression'
            href: '#compression'
          - text: 'File sharing services'
            href: '#file-sharing-services'
          - text: 'GitLab private project'
            href: '#gitlab-private-project'
          - text: "Use GNU split"
            href: "#use-gnu-split"
  hyperlinks:
    text: ''
    data: []
components:
- name: support-copy-markdown
  data:
    block:
    - header: Support Uploader
      id: support-uploader
      text: |
        A Support Engineer can use the [Support Uploader](https://gitlab.com/gitlab-com/support/support-uploader) to generate a zip bundle for a ticket where communication is ongoing with you, if you request this in the ticket.
        You will find attached in the ticket a `gs_uploader_<ticketID>.zip` bundle which includes two files, an HTML and a SH file, you can use either one to upload large files to us:
        You can either open the HTML file in your browser and attach the file using the browser form, or run `bash gs_upload.sh /path/to/filename`.
        The upload uses a pre-signed S3 URL with write-only access to a directory limited to this ticket ID, where only GitLab Support team members have access to read.
        Please feel free to inspect the code before using it, and also note that it is [open source](https://gitlab.com/gitlab-com/support/support-uploader), you can also analyze the code in the project and find a more comprehensive explanation of how it works.
        The upload currently has a 3 GiB limit per file and will expire in approximately 24 hours from the time it is generated, after which you will not be able to use it to upload other files - if you need more time, please let us know in the ticket and we can generate another one.
    - subtitle:
        text: Note
        id: US-federal-note
      text: |
        The support uploader tool is not for use with US Federal Support Portal cases. Please leverage the other methods available or reach out to the support engineer via the case for assistance uploading large files.
    - header: Other methods
      id: other-methods
      text: |
        For legacy reasons, we also list methods that were used in the past to provide large files to us. If you absolutely cannot use the Support Uploader, you can choose one of the following to share your files.
    - subtitle:
        text: Compression
        id: compression
      text: |
        If you're sending a text file or an archive with mostly text files, then please compress it. Use either bz2 (preferred) or gzip (faster) compression and it should compress to a small percentage of its original size. Zip compression is fine if you're on Windows.
        If this brings your file under the 50MB limit, then simply attach it to the ticket and be done. If not, then see below for more options.
    - subtitle:
        text: File sharing services
        id: file-sharing-services
      text: |
        Please feel free to use your own choice of file sharing service. Be aware that submitting potentially sensitive data to 3rd parties does carry a risk, so be sure to check with your security team for a properly vetted choice.
    - subtitle:
        text: GitLab private project
        id: gitlab-private-project
      text: |
        This is a fairly straight-forward option. [Sign up for a gitlab.com account](https://gitlab.com/users/sign_in#register-pane) if you don't already have one. Then create a private project and invite the Support Engineer(s) assisting you with **Reporter** access or higher. You can find the GitLab handle of the support engineer(s) you need to add by searching for the respective engineer(s) on our [team](/company/team/) page.
    - subtitle:
        text: Use GNU split
        id: use-gnu-split
      text: |
        Since the attachment is applied per file, we can split that one file into many and attach all of them to a ticket.
        The `split` command is bundled in GNU coreutils, which should be installed on all Unix-like operating systems by default. Please avoid using alternatives like winzip, winrar, 7zip, etc. We've included an example below:

        > `split -b <size> <source file> <prefix for new files>`
        > `split -b 19M source-file.tar.bz2 "target-file.tar.bz2."s`

        This will create many files in your current directory such as `target-file.tar.bz2.aa` and `target-file.tar.bz2.ab.` These files can be later joined with the `cat` command.

        > `cat target-file.tar.bz2.* > joined-file.tar.bz2`
