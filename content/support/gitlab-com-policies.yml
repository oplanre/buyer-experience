---
title: GitLab.com Specific Support Policies
description: GitLab.com Specific Support Policies
support-hero:
  data:
    title: GitLab.com Specific Support Policies
side_menu:
  anchors:
    text: "On this page"
    data:
      - text: "Account Recovery and 2FA Resets"
        href: "#account-recovery-and-2fa-resets"
      - text: "Restoration of Deleted Data"
        href: "#restoration-of-deleted-data"
      - text: "Ownership Disputes"
        href: "#ownership-disputes"
      - text: "Name Squatting Policy"
        href: "#name-squatting-policy"
      - text: "Namespace & Trademarks"
        href: "#namespace--trademarks"
      - text: "Log requests"
        href: "#log-requests"
  hyperlinks:
    text: ''
    data: []
components:
- name: support-copy-markdown
  data:
    block:
    - subtitle:
        text: Account Recovery and 2FA Resets
        id: account-recovery-and-2fa-resets
      text: |
        If you are unable to sign into your account we can help you regain access. **This service is only available for paid users** (you are part of paid group or paid user namespace).
        **Forgotten password?**

          1. Use the [Reset password form](https://gitlab.com/users/password/new)
          2. If you don't receive the reset email please [contact support](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360000803379)

        **Locked out by 2FA?**
        Are you on a free plan? [Please read this blog post](/blog/2020/08/04/gitlab-support-no-longer-processing-mfa-resets-for-free-users/)

          1. Try to [generate new recovery keys using SSH](https://docs.gitlab.com/ee/user/profile/account/two_factor_authentication.html#generate-new-recovery-codes-using-ssh)
          2. Paid users only: If you are still unable to sign in after trying step one, [contact support](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360000803379). We will request evidence to prove account ownership. We cannot guarantee to recover your account unless you pass the verification checks.

        Please note that in some cases reclaiming an account may be impossible. Read ["How to keep your GitLab account safe"](/blog/2018/08/09/keeping-your-account-safe/) for advice on preventing this.
        **Account blocked?**
        If your account has been blocked, please [contact support](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360000803379).
    - subtitle:
        text: Restoration of Deleted Data
        id: restoration-of-deleted-data
      text: |
        Any type of data restoration is currently a manual and time consuming process lead by GitLab’s infrastructure team. [Our infrastructure team clearly states](/handbook/engineering/infrastructure/faq/#q-if-a-customer-project-is-deleted-can-it-be-restored) that “once a project is deleted it cannot be restored”.
        We encourage customers to use:

          1. the [delayed deletion](https://docs.gitlab.com/ee/user/group/manage.html#enable-delayed-project-deletion) feature if it is available in your plan,
          2. [export projects and/or groups](https://docs.gitlab.com/ee/user/project/settings/import_export.html) regularly, particularly via the API.

        GitLab will consider restoration requests only when the request is for a project or group that is part of a **paid plan** with an active subscription applied, and one of the following is true:

        - The data was deleted due to a GitLab bug.
        - The organization’s contract includes a specific provision.

        Please note that user accounts and individual contributions cannot be restored.
    - subtitle:
        text: Ownership Disputes
        id: ownership-disputes
      text: |
        GitLab will not act as an arbitrator of Group or Account ownership disputes. Each user and group owner is responsible for ensuring that they are following best practices for data security.
        As GitLab subscriptions are generally business-to-business transactions, in the event that a former employee has revoked company access to a paid group, please contact GitLab Support for recovery options.
    - subtitle:
        text: Name Squatting Policy
        id: name-squatting-policy
      text: |
        Per the [GitLab Terms of Service](https://about.gitlab.com/terms/):

        > Account name squatting is prohibited by GitLab. Account names on GitLab are administered to users on a first-come, first-serve basis. Accordingly, account names cannot be held or remain inactive for future use.

        The GitLab.com Support Team will consider a [namespace](https://docs.gitlab.com/ee/user/group/#namespaces) (user name or group name) to fall under the provisions of this policy when the user has not logged in or otherwise used the namespace for an extended time.
        Namespaces will be released, if eligible under the criteria below, upon request by a member of a paid namespace or sales approved prospect.
        Specifically:

          - User namespaces can be reassigned if both of the following are true:
              1. The user's last sign in was at least two years ago.
              2. The user is not the sole owner of any active projects.
          - Group namespaces can be reassigned if one of the following is true:
              1. There is no data (no project or project(s) are empty).
              2. The owner's last sign in was at least two years ago.

        If the namespace contains data, GitLab Support will attempt to contact the owner over a two week period before reassigning the namespaces. If the namespace contains no data (empty or no projects) and the owner is inactive, the namespace will be released immediately.
        Namespaces associated with unconfirmed accounts over 90 days old are eligible for immediate release. Group namespaces that contain no data and were created more than 6 months ago are likewise eligible for immediate release.
        **NOTE:** The minimum characters required for a namespace is `2`, it is no longer possible to have a namespace of `1` character.
    - subtitle:
        text: Namespace & Trademarks
        id: namespace--trademarks
      text: |
          GitLab.com namespaces are available on a first come, first served basis and cannot be reserved. No brand, company, entity, or persons own the rights to any namespace on GitLab.com and may not claim them based on the trademark. Owning the brand "GreatCompany" does not mean owning the namespace "gitlab.com/GreatCompany". Any dispute regarding namespaces and trademarks must be resolved by the parties involved. GitLab Support will never act as arbitrators or intermediaries in these disputes and will not take any action without the appropriate legal orders.
    - subtitle:
        text: Log requests
        id: log-requests
      text: |
        Due to our [terms](/terms/), GitLab Support cannot provide raw copies of logs. However, if users have concerns, Support can answer specific questions and provide summarized information related to the content of log files.
        For paid users on GitLab.com, many actions are logged in the [Audit events](https://docs.gitlab.com/ee/administration/audit_events.html#group-events) section of your GitLab.com project or group.
