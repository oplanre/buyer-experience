---
title: Scheduling Upgrade Assistance
description: Upgrade Assistance FAQ
support-hero:
  data:
    title: Scheduling Upgrade Assistance
    content: |
      The GitLab support team is here to help. As a part of our <a href="/support/#priority-support">Priority Support</a> we offer Upgrade Assistance. 
      That is, we'll review your upgrade and rollback plans and provide 
      feedback and suggestions to help increase the likelihood of a smooth upgrade.
side_menu:
  anchors:
    text: 'On this page'
    data:
      - text: 'What is "Upgrade Assistance"?'
        href: '#what-is-upgrade-assistance'
      - text: "What information do I need to schedule Upgrade Assistance?"
        href: "#what-information-do-i-need-to-schedule-upgrade-assistance"
      - text: "How far in advance must I open a Support Request to request Upgrade Assistance?"
        href: "#how-far-in-advance-must-i-open-a-support-request-to-request-upgrade-assistance"
      - text: "Can we use custom scripts for upgrade/rollback plans?"
        href: "#can-we-use-custom-scripts-for-upgrade-rollback-plans"
      - text: "How do I schedule Upgrade Assistance?"
        href: "#how-do-i-schedule-upgrade-assistance"
        nodes:
          - text: 'Can I record the upgrade plan review session?'
            href: '#can-i-record-the-upgrade-plan-review-session'
          - text: 'Global Support'
            href: '#global-support'
          - text: 'US Federal Support'
            href: '#us-federal-support'                                   
      - text: "What does Upgrade Assistance Look like?"
        href: "#what-does-upgrade-assistance-look-like"
      - text: "What versions of GitLab will you support upgrading between?"
        href: "#what-versions-of-gitlab-will-you-support-upgrading-between"
      - text: "What if I don't give sufficient notice? Will I still be supported?"
        href: "#what-if-i-dont-give-sufficient-notice-will-i-still-be-supported"                                          
  hyperlinks:
    text: ''
    data: []
components:
- name: support-copy-markdown
  data:
    block:
      - header: What is "Upgrade Assistance"? 
        id: what-is-upgrade-assistance
        text: |
          Upgrade Assistance is part of the Priority Support package that lets you work directly with a GitLab Support Engineer during the planning of a self-hosted GitLab instance upgrade between release versions. As part of Upgrade Assistance, a GitLab Support Engineer will:

          - Review and provide feedback on the upgrade plan you provide
          - Review and provide feedback on the rollback plan you provide
          - **Optionally** you may request the engineer host a 30 minute screen share session to review the upgrade and rollback plans synchronously with you. 
              This must be requested and [scheduled with the engineer in advance](/support/scheduling-upgrade-assistance/#how-far-in-advance-must-i-open-a-support-request-to-request-upgrade-assistance). 
              The purpose of the call is to provide final review and help familiarize you with 
              the steps prior to initiating the upgrade. Engineers will not be engaged synchronously during the upgrade timeframe.
            
          "Upgrade" in this sense does not include:

          - migrations between GitLab distributions (e.g. GitLab CE to GitLab EE)
          - migrations between GitLab architectures (e.g. GitLab Omnibus to GitLab Helm)
          - scaling current architecture (e.g. GitLab 5k Reference architecture to GitLab 20k Reference Architecture)
          - moving data between GitLab installations (e.g. migrating from GitLab self-managed to GitLab.com)
          - adding additional GitLab features (e.g. adding a GitLab Geo replica)

          In short, Upgrade Assistance is simply for moving from one GitLab point release to a newer GitLab point release on the same server.
      - header: What information do I need to schedule Upgrade Assistance?
        id: what-information-do-i-need-to-schedule-upgrade-assistance
        text: |
          First, confirm that nothing about your instance would make the request [out of scope for support](/support/statement-of-support.html#out-of-scope). 
          Then, please provide all the relevant information you can so that we will be best positioned to assist you. At a minimum, we require:

          1. An [upgrade plan](https://docs.gitlab.com/ee/update/plan_your_upgrade.html#upgrade-plan)
          2. A [rollback plan](https://docs.gitlab.com/ee/update/plan_your_upgrade.html#rollback-plan)
          3. Updated architecture documentation
          4. The point of contact for support to use (email address preferred)
          5. The time of the upgrade (please include date, time, and timezone)
          6. Any additional relevant information (e.g. We've had no issues simulating this upgrade in our staging environment)
          
          See the [create a GitLab upgrade plan](https://docs.gitlab.com/ee/update/plan_your_upgrade.html) documentation for further advice on planning an upgrade.
      - header: How far in advance must I open a Support Request to request Upgrade Assistance?
        id: how-far-in-advance-must-i-open-a-support-request-to-request-upgrade-assistance
        text: |
          For support to properly assist you, the earlier you can notify us and include all of the information we need, the better. After providing the necessary information needed in order to request Upgrade Assistance, our minimum requirements for notification are:
          
          - Upgrades should be scheduled at least one week after support has confirmed they have received all relevant upgrade documents.
          - Two weeks notice once all relevant upgrade documents are received is required for all upgrades involving the optional 30 minute synchronous upgrade plan review session
          
          If you cannot meet our minimum advanced notice period for your planned upgrade, we may recommend postponing the upgrade attempt. If you choose to proceed with the originally scheduled date:

          - Should time permit we will get a support engineer to review your upgrade and rollback plans.
          - You still have [access to emergency support](https://about.gitlab.com/support/#how-to-trigger-emergency-support) should you encounter a production outage as a result of your upgrade.
      - header: Can we use custom scripts for upgrade/rollback plans?
        id: can-we-use-custom-scripts-for-upgrade-rollback-plans
        text: |
          You can do so, however we cannot review the scripts themselves to determine if they are viable. Generally speaking, our upgrade documentation is the single source of truth for how to best carry out an upgrade.
          If issues do occur during the upgrade window and you are running a custom script, it is likely the advice from support will be to utilize your rollback plan. It is important to know that should issues arise while using custom scripts, Support will recommend following the exact steps from our documentation on future attempts.
      - header: How do I schedule Upgrade Assistance? 
        id: how-do-i-schedule-upgrade-assistance
        subtitle: 
          text: Can I record the upgrade plan review session?
          id: can-i-record-the-upgrade-plan-review-session
        text: |
          Frequently during screenshare sessions plaintext secrets or other sensitive information can be displayed. To ensure sure that any recordings that inadvertantly contain this information stay within your security boundary, we ask that customers initiate and store any recordings.
          If you wish to record the session, either request the engineer(host) to grant you the ownership of the call to start the recording process or invite the respective engineer to a call where you can initiate the recording from your end.
      - subtitle:
          text: Global Support
          id: global-support
        text: |
          Organizations with [Priority Support](/support/#priority-support) or higher may use the [Support for Self Managed instances](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=426148) form and under "Problem Type" select "Upgrade Assistance Request" to begin the process. For efficiency, please include the [required information](/support/scheduling-upgrade-assistance/#what-information-do-i-need-to-schedule-upgrade-assistance) when opening the ticket.
      - subtitle:
          text: US Federal Support
          id: us-federal-support
        text: |
          If your organization meets the [requirements](/support/#limitations) for GitLab's US Federal Support you may use the [US Federal Upgrade Planning Assistance Request](https://federal-support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360001434131) form to begin the process. For efficiency, please include the [required information](/support/scheduling-upgrade-assistance/#what-information-do-i-need-to-schedule-upgrade-assistance) when opening the ticket.
      - header: What does Upgrade Assistance Look like?
        id: what-does-upgrade-assistance-look-like
        text: |
          Once a case is filed with the relevant support team it will be triaged by a support engineer who will verify that all of the requested information was provided. The engineer will then review the upgrade, rollback, and architecture plans, making suggestions or notes on additional steps or other concerns to be aware of. If requested, the engineer can provide a single use Calendly link to schedule a review call **in advance** of when you plan to run your upgrade to answer any final questions you may have. Note that the review call will not be offered until the engineer has all of the available information required. We recommend scheduling the review call at least 3 business days ahead of your scheduled upgrade. Please be as comprehensive as possible when opening the ticket to avoid delays.
          After confirming that all relevant information has been provided, and thorough review has been completed with all questions and concerns addressed you should be ready to begin your GitLab upgrade!
          If you have requested the optional 30 minute upgrade plan review session with an engineer, a GitLab Support Engineer will join you at the scheduled meeting time to help kick things off and ensure that you're set up for success by:
          
          1. Ensuring that there's an open ticket for the upgrade that they are monitoring
          2. Going over the upgrade plan once more
          3. Verifying that there is a rollback plan in place should things not go according to plan.
          
          After 30 minutes the engineer will drop off the call and may be available via the case asynchronously for any questions that may arise.
          Once the upgrade is complete, and has passed your post-upgrade success criteria, please be sure to send an update to the ticket that was opened so the Engineer knows they can resolve the case.
          If there haven't been any updates for some time, the Engineer assisting may send an update to the ticket requesting a status check in.
          If you run into a production impacting issue while performing your upgrade and cannot proceed you may [page the on-call engineer](/support/#how-to-trigger-emergency-support). 
          The on-call engineer may invoke your rollback plan in order to avoid further impact to production availability.
      - header: What versions of GitLab will you support upgrading between?
        id: what-versions-of-gitlab-will-you-support-upgrading-between
        text: |
          As noted in our Statement of Support, we [support the current major version and two previous major versions](/support/statement-of-support/#version-support). If you're upgrading from a version of GitLab that is no longer supported, Upgrade Assistance may not be an option. If you're in this situation, please discuss options with your Technical Account Manager or your Account Manager for Professional Services options.
      - header: What if I don't give sufficient notice? Will I still be supported?
        id: what-if-i-dont-give-sufficient-notice-will-i-still-be-supported
        text: |
          As a part of [Priority Support](/support/#priority-support), you're also entitled to **24x7 uptime Support** If you encounter any issues that are leading to downtime during your upgrade, you can page the on-call engineer to help troubleshoot.
          Please provide as much context as you can, including an upgrade plan when you [open your emergency ticket](https://about.gitlab.com/support/#how-to-trigger-emergency-support).
          Do note, that in some cases the best option available may be to invoke your rollback plan and reschedule the upgrade.
